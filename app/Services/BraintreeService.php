<?php
namespace App\Services;

use App\Models\BrainTree;

class BraintreeService
{

    public static function getInstance()
    {
        $braintree = BrainTree::all()->first();

        return new \Braintree\Gateway([
            'environment' => $braintree->environment,
            'merchantId' => $braintree->merchant_id,
            'publicKey' => $braintree->public_key,
            'privateKey' => $braintree->private_key
        ]);
    }
}
