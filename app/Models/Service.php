<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'id',
        'name',
        'single_spacing_price',
        'double_spacing_price',
        'inactive'
    ];
}
