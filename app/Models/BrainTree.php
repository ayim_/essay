<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrainTree extends Model
{
    protected $casts = [
        'is_paypal_enabled' => 'boolean'
    ];

    protected $fillable = [
        'environment',
        'merchant_id',
        'public_key',
        'private_key',
        'is_paypal_enabled'
    ];
}
