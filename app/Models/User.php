<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\TagOperation;
use Spatie\Activitylog\Traits\CausesActivity;

class User extends Authenticatable
{
    use Notifiable, HasRoles, CausesActivity, TagOperation;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function my_orders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }

    public function tasks()
    {
        return $this->hasMany(Order::class, 'staff_id', 'id');
    }

    public function ratings_received()
    {
        return $this->hasManyThrough(Rating::class, Order::class, 'staff_id');
    }

    public function unbilled_tasks()
    {
        return $this->tasks()
            ->where('order_status_id', ORDER_STATUS_COMPLETE)
            ->whereNull('billed');
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function pushNotification()
    {
        return $this->hasOne(PushNotification::class);
    }


    public function uncleared_payment_requests()
    {
        return $this->bills()->whereNull('paid');
    }

    public function uncleared_payment_total()
    {
        return $this->uncleared_payment_requests()
            ->get()
            ->sum('total');
    }

    public function records()
    {
        return $this->hasMany(UserRecord::class);
    }

    public function setMetaData()
    {
        $records = $this->records()->get();

        $this->meta = new \StdClass();

        if ($records->count() > 0) {
            foreach ($records as $row) {
                $this->meta->{$row->option_key} = $row->option_value;
            }
        }
    }

    public function meta($option_key)
    {
        $records = $this->records()
            ->where('option_key', $option_key)
            ->get();

        if ($records->count() > 0) {
            return optional($records->first())->option_value;
        }
    }
}
