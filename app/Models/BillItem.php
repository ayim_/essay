<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'total',
        'bill_id'
    ];

    function bill()
    {
        return $this->belongsTo(Bill::class);
    }

    function order()
    {
        return $this->belongsTo(Order::class);
    }
}
