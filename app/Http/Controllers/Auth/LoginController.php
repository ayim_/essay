<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validateLogin(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user && $user->inactive) {
            throw ValidationException::withMessages([
                $this->username() => __('The Account in Suspended!')
            ]);
        }
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string'
        ]);
    }

    public function authenticate(Request $request, $user)
    {
        $intended = redirect()->intended()->getTargetUrl();

        if ($intended == route('frontend.orders')) {
            return redirect()->route('frontend.orders');
        }

        return redirect()->route(get_default_route_by_user($user));
    }
}
